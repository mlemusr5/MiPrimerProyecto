# Generated by Django 4.2.5 on 2023-09-23 03:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('paises', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Departamento',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=70)),
                ('pais', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='paises.paises')),
            ],
        ),
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=70)),
                ('departamento', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='paises.departamento')),
            ],
        ),
    ]

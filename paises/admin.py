from django.contrib import admin
from .models import Paises, Departamento, Municipio

# Register your models here.

admin.site.register(Paises)
admin.site.register(Departamento)
admin.site.register(Municipio)
from typing import Any
from django import http
from django.shortcuts import render
from django.views.generic import TemplateView
from .models import Paises, Departamento, Municipio
# Create your views here.

class InicioVista(TemplateView):
    template_name= 'inicio/inicio.html'

    def get(self, request):
        data={}
        try:
            paises= Paises.objects.all()
            data['paises']= paises
            data['error']= 0 if len(paises)>0 else 1
        except:
            data['error']=1
        return render(request, self.template_name, data)
    
class VistaDepartamentos(TemplateView):
    template_name= 'inicio/departamentos.html'

    def get(self, request):
        data={}
        try:
            departamentos= Departamento.objects.all()
            data['departamentos']= departamentos
            data['error']= 0 if len(departamentos)>0 else 1
        except:
            data['error']=1
        return render(request, self.template_name, data)
    
class VistaMunicipios(TemplateView):
    template_name= 'inicio/municipios.html'

    def get(self, request):
        data={}
        try:
            municipios= Municipio.objects.all()
            data['municipios']= municipios
            data['error']= 0 if len(municipios)>0 else 1
        except:
            data['error']=1
        return render(request, self.template_name, data)
from django.db import models

# Create your models here.

class Paises (models.Model):
    nombre = models.CharField(max_length=70)

class Departamento (models.Model):
    nombre = models.CharField(max_length=70)
    pais = models.ForeignKey(Paises,on_delete=models.CASCADE)

class Municipio (models.Model):
    nombre = models.CharField(max_length=70)
    departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)